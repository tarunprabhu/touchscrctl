# touchscrctl

`touchscrctl` manages a touchscreen. This is intended to provide a more 
familiar interface to libinput and is largely consistent with the other
utilities. 

# Requirements

`touchscrctl` manages the touchscreen using [libinput](https://github.com/wayland-project/libinput).
libinput should already be installed on your system, but if it is isn't, it is 
probably best to install it using the system's package manager. `touchscrctl`
is written in Lua and requires a Lua interpreter and some Lua packages. 

## Interpreter

Lua >= 5.2

## Packages

These can be installed using [LuaRocks](https://luarocks.org). They may 
also be present in the system's repositories.

- [argparse](https://github.com/mpeterv/argparse)
- [luaposix](https://github.com/luaposix/luaposix)

# Installation

The `touchscrctl` script can be downloaded separately and copied to a directory 
that is in $PATH. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
touchscrctl -h
```
